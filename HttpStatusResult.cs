﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace System.Web.Mvc {
    public class HttpStatusResult : HttpStatusCodeResult {

        public string Message {
            get;
            private set;
        }

        public ActionResult InnerResult {
            get;
            set;
        }

        protected HttpStatusResult(HttpStatusCode status)
            : base((int) status) {

        }

        public HttpStatusResult(HttpStatusCode status, string message) : this(status) {
            this.Message = message;
        }

		public HttpStatusResult(HttpStatusCode status, object json)
			: base(status){
			this.InnerResult = new JsonNetResult {
				Data = json,
			};
		}

        public override void ExecuteResult(ControllerContext context) {

            if (null != InnerResult) InnerResult.ExecuteResult(context);
            else if(!string.IsNullOrWhiteSpace(this.Message)) context.HttpContext.Response.Write(this.Message);

            base.ExecuteResult(context);

            

        }
    }
}
