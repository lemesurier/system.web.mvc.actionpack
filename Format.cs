﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace System.Web
{
    public enum Format
    {
        Unknown,
        Html,
        Html4,
        Html5,
        Xhtml,
        Xhtml5,
        JavaScript,
        Json,
        Xml,
        Text,
        EMail,
        Png,
        Gif,
        Jpeg,
        Rss,
        Atom,
        Css,
        Swf,
        vCard,
        Csv,
        Pdf
    }

    public static class FormatExtensions {

        public static string ToMimeType(this Format format) {
            return FormatMap.GetMimeType(format);
        }

    }

    internal static class FormatMap
    {
        private static Dictionary<string, Format> _extFormatMap;
        private static Dictionary<string, Format> _mimeFormatMap;
        private static Dictionary<Format, string> _formatMimeMap;
        private static Dictionary<Format, string> _formatExtMap;
        private static List<string> _mimeOrder;

        static FormatMap()
        {
            _extFormatMap = new System.Collections.Generic.Dictionary<string,Format>();
            _mimeFormatMap = new Dictionary<string, Format>();
            _formatMimeMap = new Dictionary<Format, string>();
            _formatExtMap = new Dictionary<Format, string>();
            _mimeOrder = new List<string>();

            // sticking with html as the default
            _mimeFormatMap.Add("text/html", Format.Html);
            _extFormatMap.Add(".html", Format.Html);
            _extFormatMap.Add(".htm", Format.Html);
            _formatMimeMap.Add(Format.Html, "text/html");
            _formatExtMap.Add(Format.Html, ".html");
            _formatMimeMap.Add(Format.Html4, "text/html");
            _formatExtMap.Add(Format.Html4, ".html");
            _formatMimeMap.Add(Format.Html5, "text/html");
            _formatExtMap.Add(Format.Html5, ".html5");
            _mimeOrder.Add("text/html");

            _mimeFormatMap.Add("application/xhtml+xml", Format.Xhtml);
            _formatMimeMap.Add(Format.Xhtml, "application/xhtml+xml");
            _formatExtMap.Add(Format.Xhtml, ".xhtml");
            _formatMimeMap.Add(Format.Xhtml5, "application/xhtml+xml");
            _formatExtMap.Add(Format.Xhtml5, ".xhtml5");
            _mimeOrder.Add("application/xhtml+xml");

            _extFormatMap.Add(".js", Format.JavaScript);
            _mimeFormatMap.Add("text/javascript", Format.JavaScript);
            _mimeFormatMap.Add("application/x-javascript", Format.JavaScript);
            _mimeFormatMap.Add("application/javascript", Format.JavaScript);
            _formatMimeMap.Add(Format.JavaScript, "application/javascript");
            _formatExtMap.Add(Format.JavaScript, ".js");
            _mimeOrder.Add("text/javascript");

            _extFormatMap.Add(".json", Format.Json);
            _mimeFormatMap.Add("application/json", Format.Json);
            _formatMimeMap.Add(Format.Json, "application/json");
            _formatExtMap.Add(Format.Json, ".json");
            _mimeOrder.Add("application/json");

            _extFormatMap.Add(".rss", Format.Rss);
            _mimeFormatMap.Add("application/rss+xml", Format.Rss);
            _formatMimeMap.Add(Format.Rss, "application/rss+xml");
            _formatExtMap.Add(Format.Rss, ".rss");
            _mimeOrder.Add("application/rss+xml");

            _extFormatMap.Add(".atom", Format.Atom);
            _formatMimeMap.Add(Format.Atom, "application/atom+xml");
            _formatExtMap.Add(Format.Atom, ".atom");
            _mimeOrder.Add("application/atom+xml");

            _extFormatMap.Add(".xml", Format.Xml);
            _mimeFormatMap.Add("text/xml", Format.Xml);
            _mimeFormatMap.Add("application/xml", Format.Xml);
            _formatMimeMap.Add(Format.Xml, "text/xml");
            _formatExtMap.Add(Format.Xml, ".xml");
            _mimeOrder.Add("application/xml");
            
            _extFormatMap.Add(".txt", Format.Text);
            _mimeFormatMap.Add("text/plain", Format.Text);
            _formatMimeMap.Add(Format.Text, "text/plain");
            _formatExtMap.Add(Format.Text, ".txt");
            _mimeOrder.Add("text/plain");

            _extFormatMap.Add(".png", Format.Png);
            _mimeFormatMap.Add("image/png", Format.Png);
            _formatMimeMap.Add(Format.Png, "image/png");
            _formatExtMap.Add(Format.Png, ".png");
            _mimeOrder.Add("image/png");

            _extFormatMap.Add(".gif", Format.Gif);
            _mimeFormatMap.Add("image/gif", Format.Gif);
            _formatMimeMap.Add(Format.Gif, "image/gif");
            _formatExtMap.Add(Format.Gif, ".gif");
            _mimeOrder.Add("image/gif");

            _extFormatMap.Add(".jpg", Format.Jpeg);
            _extFormatMap.Add(".jpeg", Format.Jpeg);
            _mimeFormatMap.Add("image/jpeg", Format.Jpeg);
            _formatMimeMap.Add(Format.Jpeg, "image/jpeg");
            _formatExtMap.Add(Format.Jpeg, ".jpg");
            _mimeOrder.Add("image/jpeg");

            _extFormatMap.Add(".css", Format.Css);
            _mimeFormatMap.Add("text/css", Format.Css);
            _formatMimeMap.Add(Format.Css, "text/css");
            _formatExtMap.Add(Format.Css, ".css");
            _mimeOrder.Add("text/css");

            _extFormatMap.Add(".swf", Format.Swf);
            _mimeFormatMap.Add("application/x-shockwave-flash", Format.Swf);
            _formatMimeMap.Add(Format.Swf, "application/x-shockwave-flash");
            _formatExtMap.Add(Format.Swf, ".swf");
            _mimeOrder.Add("application/x-shockwave-flash");

            _extFormatMap.Add(".vcard", Format.vCard);
            _extFormatMap.Add(".vcf", Format.vCard);
            _mimeFormatMap.Add("text/x-vcard", Format.vCard);
            _mimeFormatMap.Add("text/directory;profile=vCard", Format.vCard);
            _mimeFormatMap.Add("text/directory", Format.vCard);
            _formatMimeMap.Add(Format.vCard, "text/x-vcard");
            _formatExtMap.Add(Format.vCard, ".vcf");
            _mimeOrder.Add("text/x-vcard");

            _extFormatMap.Add(".csv", Format.Csv);
            _mimeFormatMap.Add("text/csv", Format.Csv);
            _formatMimeMap.Add(Format.Csv, "text/csv");
            _formatExtMap.Add(Format.Csv, ".csv");
            _mimeOrder.Add("text/csv");

            _extFormatMap.Add(".pdf", Format.Pdf);
            _mimeFormatMap.Add("application/pdf", Format.Pdf);
            _formatMimeMap.Add(Format.Pdf, "application/pdf");
            _formatExtMap.Add(Format.Pdf, ".pdf");
            _mimeOrder.Add("application/pdf");

            //_mimeFormatMap.Add("text/*", Format.Html);
            //_mimeOrder.Add("text/*");

            //_mimeFormatMap.Add("*/*", Format.Html);
            //_mimeOrder.Add("*/*");

            
        }

        public static Format GetExtensionFormat(string extension)
        {
            extension = extension.ToLower();
            Format format;
            return _extFormatMap.TryGetValue(extension, out format) ? format : Format.Unknown;
        }

        public static Format GetRequestTypeFormat(string[] acceptedTypes)
        {

            if (acceptedTypes != null)
            {

                foreach (string key in _mimeOrder)
                {

                    foreach (string a in acceptedTypes)
                    {
                        if (a.StartsWith(key, StringComparison.OrdinalIgnoreCase))
                        {
                            return _mimeFormatMap[key];
                        }
                    }

                }
            }

            return Format.Unknown;
        }

        // intelligently try to figure out what's the accepted mime type for the given format
        public static string GetAcceptedMimeTypeForFormat(Format format, string[] acceptedTypes) {

            if (acceptedTypes != null) {

                foreach (string accepted in acceptedTypes) {

                    if(accepted == "*/*") {
                        continue;
                    }
                    if (accepted.EndsWith("/*")) {

                        // this matches things like text/*
                        // loop through all of the mime types for this format and see if any match the first part
                        string acceptedClass = accepted.Substring(0, accepted.IndexOf('/') + 1);
                        foreach (KeyValuePair<string, Format> pair in _mimeFormatMap) {
                            if (pair.Value == format && pair.Key.StartsWith(acceptedClass) && !pair.Key.EndsWith("/*")) {
                                return pair.Key;
                            }
                        }


                    } else {
                        Format explicitMatch;
                        if (_mimeFormatMap.TryGetValue(accepted, out explicitMatch) && explicitMatch == format) {
                            return accepted;
                        }
                    }
                }

                foreach (string key in _mimeOrder) {

                    foreach (string a in acceptedTypes) {
                        if (a.StartsWith(key, StringComparison.OrdinalIgnoreCase)) {
                            GetMimeType(_mimeFormatMap[key]);
                        }
                    }

                }
            }

            // return the default mime type for the format
            return GetMimeType(format);

        }

        public static Format GetContentTypeFormat(string contentType) {
            string mime2 = contentType.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries)[0];
            Format format;
            return _mimeFormatMap.TryGetValue(mime2, out format) ? format : Format.Unknown;
        }

        public static Format GetMimeTypeFormat(string mimeType)
        {
            //string mime2 = mimeType.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries)[0];
            Format format;
            return _mimeFormatMap.TryGetValue(mimeType, out format) ? format : Format.Unknown;
        }

        public static string GetMimeType(Format format)
        {
            string mime;
            return _formatMimeMap.TryGetValue(format, out mime) ? mime : "";
        }
        

        public static string GetFormatDefaultExtension(Format format) {
            string ext;
            return _formatExtMap.TryGetValue(format, out ext) ? ext : null;
        }

    }

    public static class FormatHelper
    {

        //private static string formatCacheName = typeof(Format).ToString();

        public static Format GetFormat(HttpRequest request)
        {
            Format format = Format.Unknown;
            string filePath = request.RawUrl.Split(new char[] { '?' }, StringSplitOptions.RemoveEmptyEntries)[0];
            if(System.IO.Path.HasExtension(filePath))
            {
                format = FormatMap.GetExtensionFormat(System.IO.Path.GetExtension(filePath));
            }
            if (format == Format.Unknown)
            {
                format = FormatMap.GetRequestTypeFormat(request.AcceptTypes);
            }
            return format;
        }

        public static Format GetFormat(HttpRequestBase request) {
            Format format = Format.Unknown;
            string filePath = request.RawUrl.Split(new char[] { '?' }, StringSplitOptions.RemoveEmptyEntries)[0];
            if (System.IO.Path.HasExtension(filePath)) {
                format = FormatMap.GetExtensionFormat(System.IO.Path.GetExtension(filePath));
            }
            if (format == Format.Unknown) {
                format = FormatMap.GetRequestTypeFormat(request.AcceptTypes);
            }
            return format;
        }

        public static Format GetFormat(string mimeType)
        {
            return FormatMap.GetMimeTypeFormat(mimeType);
        }

        /*public static Format GetExtensionFormat(string mimeType) {
            return FormatMap.GetExtensionFormat(mimeType);
        }*/

        public static string GetContentType(Format format)
        {
            return FormatMap.GetMimeType(format);
        }

        //public static Format GetResponseFormat(HttpContext context) {
        //    if (context.Items.Contains(formatCacheName)) {
        //        return (Format)context.Items[formatCacheName];
        //    }
        //    return Format.Unknown;
        //}

        //public static Format GetResponseFormat(HttpContextBase context) {
        //    if (context.Items.Contains(formatCacheName)) {
        //        return (Format)context.Items[formatCacheName];
        //    }
        //    return Format.Unknown;
        //}

        //public static void ReformatResponse(HttpContextBase context, Format format) {
        //    context.Items[formatCacheName] = format;
        //    //context.Response.ContentType = GetContentType(format);
        //}

        //public static void ReformatResponse(HttpContext context, Format format) {
        //    context.Items[formatCacheName] = format;
        //    //context.Response.ContentType = GetContentType(format);
        //}

        //public static void EnsureResponseFormat(HttpContext context) {

        //    Format format = GetResponseFormat(context);

        //    // force the content type
        //    if (format != Format.Unknown) {
        //        context.Response.ContentType = FormatHelper.GetContentType(format);
        //    }
        //}

        //public static void EnsureResponseFormat(HttpContextBase context) {
        //    Format format = GetResponseFormat(context);

        //    // force the content type
        //    if (format != Format.Unknown) {
        //        context.Response.ContentType = FormatHelper.GetContentType(format);
        //    }
        //}
    }
}
