﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace System.Web.Mvc {
    public class HttpBadRequestResult : HttpStatusResult {

        public HttpBadRequestResult()
            : this(null) {

        }

        public HttpBadRequestResult(string message)
            : base(HttpStatusCode.BadRequest, message) {
        }

		public HttpBadRequestResult(object json)
			: base(HttpStatusCode.BadRequest, json){
		}

        public override void ExecuteResult(ControllerContext context) {
            base.ExecuteResult(context);

            //if(!string.IsNullOrWhiteSpace(this.Message)) context.HttpContext.Response.Write(this.Message);

        }
    }
}
