﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.Mvc {
    public static class ActionPacksUrlHelperExtensions {

        public static string Content(this UrlHelper @this, string host, string path) {
            return host + @this.Content(path);
        }
    }
}
