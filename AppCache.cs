﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace System.Web {
    public class AppCache {

        public static T Set<T>(string key, T value) {
            return Set(key, value, null);
        }

        public static T Set<T>(string key, T value, SqlDependency dependency) {
            if (key.IsNullOrWhiteSpace() || value == null) return default(T);
            var policy = new CacheItemPolicy();
            if (dependency != null) policy.ChangeMonitors.Add(new SqlChangeMonitor(dependency));

            ObjectCache cache = MemoryCache.Default;
            cache.Set(key, value, policy);

            return value;
        }

        public static object Get(string key) {

            ObjectCache cache = MemoryCache.Default;
            return cache.Get(key);
        }

        /*public static T Get<T>(string key) {

            ObjectCache cache = MemoryCache.Default;
            object val = cache.Get(key);
            if (val != null && typeof(T).IsAssignableFrom(val.GetType())) {
                return (T)val;
            }

            return default(T);
        }*/

        public static ObjectCache Cache {
            get {
                return MemoryCache.Default;
            }
        }

        public static T Get<T>(string key) where T : class {

            ObjectCache cache = MemoryCache.Default;
            return cache.Get(key) as T;

        }

        public static T Get<T>(string key, Action<string> onGet, Func<string, T> create) where T : class {
            T val = Get<T>(key);
            if (val == null) return Set<T>(key, create(key));
            onGet(key);
            return val;
        }

        public static T Get<T>(string key, Func<string, T> create) where T : class {
            T val = Get<T>(key);
            if (val == null) return Set<T>(key, create(key));
            return val;
        }

    }
}
