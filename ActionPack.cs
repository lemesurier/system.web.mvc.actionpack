﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.Mvc {
    public static class ActionPack {
        public static ModelBinderDictionary RegisterModelBinders(ModelBinderDictionary binders) {
            return binders
                .For<System.Security.Claims.ClaimsPrincipal>(new IPrincipalModelBinder<System.Security.Claims.ClaimsPrincipal>())
                .For<System.Security.Principal.IPrincipal>(new IPrincipalModelBinder())
                .For<Guid>(new ShortGuidModelBinder())
                .For<Guid?>(new ShortGuidModelBinder())
                .For<ShortGuid>(new ShortGuidModelBinder())
                .For<ShortGuid?>(new ShortGuidModelBinder())
                .For<Format>(new FormatModelBinder());
        }

        public static GlobalFilterCollection RegisterGlobalFilters(GlobalFilterCollection filters) {
            filters.Add(new JsonpAttribute());
            return filters;
        }
    }
}
