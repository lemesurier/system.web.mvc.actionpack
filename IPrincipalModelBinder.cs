﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.Mvc {

    public class IPrincipalModelBinder : IModelBinder {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {
            if (controllerContext == null) {
                throw new ArgumentNullException("controllerContext");
            }
            if (bindingContext == null) {
                throw new ArgumentNullException("bindingContext");
            }
            return BindPrincipal(controllerContext.HttpContext);
        }

        public IPrincipal BindPrincipal(HttpContextBase httpContext) {
            IPrincipal p = httpContext.User;
            return p;
        }
    }

    public class IPrincipalModelBinder<T> : IModelBinder where T:IPrincipal {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {
            if (controllerContext == null) {
                throw new ArgumentNullException("controllerContext");
            }
            if (bindingContext == null) {
                throw new ArgumentNullException("bindingContext");
            }
            return BindPrincipal(controllerContext.HttpContext);
        }

        public T BindPrincipal(HttpContextBase httpContext) {
            return (T) httpContext.User;
        }
    }

}
