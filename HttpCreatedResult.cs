﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace System.Web.Mvc {
    public class HttpCreatedResult : HttpStatusResult {

		private const HttpStatusCode status = HttpStatusCode.Created; 

        public Uri Location {
            get;
            set;
        }

        /*public ActionResult InnerResult {
            get;
            set;
        }*/

        public HttpCreatedResult()
            : base(status) {

        }

		public HttpCreatedResult(object json)
			: base(status, json) {
		}

        public HttpCreatedResult(Uri location)
            : this() {
            this.Location = location;
        }

        public override void ExecuteResult(ControllerContext context) {

            //if (null != InnerResult) InnerResult.ExecuteResult(context);

            base.ExecuteResult(context);

            if(this.Location != null && this.Location.IsAbsoluteUri) context.HttpContext.Response.RedirectLocation = this.Location.ToString();

        }
    }
}
