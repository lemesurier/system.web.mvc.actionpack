﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.Mvc {
    public class ActionPackController : Controller {
        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding) {
            return new JsonNetResult {
                Data = data,
                ContentEncoding = contentEncoding,
                ContentType = contentType
            };
        }
        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior) {
            return new JsonNetResult {
                Data = data,
                ContentEncoding = contentEncoding,
                ContentType = contentType,
                JsonRequestBehavior = behavior
            };
        }
        public static JsonResult JsonResult(object data) {
            return new JsonNetResult {
                Data = data,
                //ContentEncoding = contentEncoding,
                //ContentType = contentType,
                //JsonRequestBehavior = behavior,
            };
        }
    }
}
