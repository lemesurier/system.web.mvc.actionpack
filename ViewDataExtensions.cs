﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace System.Web.Mvc {
    public static class ViewDataExtensions {

        public static object EvalOrDefault(this ViewDataDictionary viewData, string expression, object defaultValue) {
            object result = viewData.Eval(expression);
            return result ?? defaultValue;
        }
        public static T EvalAsType<T>(this ViewDataDictionary viewData, string expression) {
            object val = viewData.Eval(expression);
            if (val != null && typeof(T).IsAssignableFrom(val.GetType())) {
                return (T)val;
            }
            return default(T);
        }

        public static T EvalAsType<T>(this ViewDataDictionary viewData, string expression, T defaultValue) {
            object val = viewData.Eval(expression);
            if (val != null && typeof(T).IsAssignableFrom(val.GetType())) {
                return (T)val;
            }
            return defaultValue;
        }
    }
}