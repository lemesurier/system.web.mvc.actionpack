﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.Mvc {
    public class MvcHttpApplication : System.Web.HttpApplication {

        public override void Init() {
            base.Init();

            MvcHandler.DisableMvcResponseHeader = true;

            // strip headers
            this.PreSendRequestHeaders += delegate(Object sender, EventArgs e) {
                new List<string> { "Server", "X-AspNet-Version", "X-AspNetMvc-Version", "X-Powered-By" }.ForEach(h => ((HttpApplication)sender).Context.Response.Headers.Remove(h));
            };
        }
    }

    public class ActionPackApplication : MvcHttpApplication {
        public override void Init() {

            ActionPack.RegisterModelBinders(ModelBinders.Binders);
            ActionPack.RegisterGlobalFilters(GlobalFilters.Filters);

            base.Init();

            
        }
    }
}
