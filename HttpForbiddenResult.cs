﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace System.Web.Mvc {
    public class HttpForbiddenResult : HttpStatusResult {


        public HttpForbiddenResult()
            : this(null) {

        }

        public HttpForbiddenResult(string message)
            : base(HttpStatusCode.Forbidden, message) {
        }

		public HttpForbiddenResult(object json)
			: base(HttpStatusCode.Forbidden, json) {
		}

        public override void ExecuteResult(ControllerContext context) {
            base.ExecuteResult(context);

            //if(!string.IsNullOrWhiteSpace(this.Message)) context.HttpContext.Response.Write(this.Message);

        }
    }
}
