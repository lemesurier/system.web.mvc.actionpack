﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace System.Web.Mvc {
    public class ShortGuidModelBinder : CustomModelBinderAttribute, IModelBinder {

            public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {
                //string id = new[] { bindingContext.ModelName }.Select(key => key.IsNullOrWhiteSpace() ? null : bindingContext.ValueProvider.GetValue(key)).Select<ValueProviderResult, string>(result => result != null ? result.AttemptedValue : null).FirstOrDefault(s => !s.IsNullOrWhiteSpace());

                Guid? g;
                if ((g = BindGuid(controllerContext, bindingContext)).HasValue) return g.Value;
                //if(!id.IsNullOrWhiteSpace() && ShortGuid.TryParseGuid(id, out g)) return g;

                // try url decoding
                //if (!id.IsNullOrWhiteSpace() && ShortGuid.TryParseGuid(controllerContext.HttpContext.Server.UrlDecode(id), out g)) return g;

                return null;

            }

            public override IModelBinder GetBinder() {
                return this;
            }

            public static Guid? BindGuid(string value) {
                Guid g;
                if (!value.IsNullOrWhiteSpace() && ShortGuid.TryParseGuid(value, out g)) return g;

                return null;
            }

            private Guid? BindGuid(string value, HttpContextBase httpContext) {

                //httpContext.Trace.Write("bind guid \"" + value + "\"");

                return BindGuid(value);
            }

            public Guid? BindGuid(ModelBindingContext bindingContext) {
                string id = new[] { bindingContext.ModelName }.Select(key => key.IsNullOrWhiteSpace() ? null : bindingContext.ValueProvider.GetValue(key)).Select<ValueProviderResult, string>(result => result != null ? result.AttemptedValue : null).FirstOrDefault(s => !s.IsNullOrWhiteSpace());

                /*Guid g;
                //if(!id.IsNullOrWhiteSpace() && ShortGuid.TryParseGuid(id, out g)) return g;

                // try url decoding
                if (!id.IsNullOrWhiteSpace() && ShortGuid.TryParseGuid(controllerContext.HttpContext.Server.UrlDecode(id), out g)) return g;*/

                return BindGuid(id);

            }

        public static Guid? Bind(ModelBindingContext bindingContext) {
            string id = new[] { bindingContext.ModelName }.Select(key => key.IsNullOrWhiteSpace() ? null : bindingContext.ValueProvider.GetValue(key)).Select<ValueProviderResult, string>(result => result != null ? result.AttemptedValue : null).FirstOrDefault(s => !s.IsNullOrWhiteSpace());

            return BindGuid(id);

        }


        private Guid? BindGuid(ControllerContext controllerContext, ModelBindingContext bindingContext) {
                string id = new[] { bindingContext.ModelName }.Select(key => key.IsNullOrWhiteSpace() ? null : bindingContext.ValueProvider.GetValue(key)).Select<ValueProviderResult, string>(result => result != null ? result.AttemptedValue : null).FirstOrDefault(s => !s.IsNullOrWhiteSpace());

                /*Guid g;
                //if(!id.IsNullOrWhiteSpace() && ShortGuid.TryParseGuid(id, out g)) return g;

                // try url decoding
                if (!id.IsNullOrWhiteSpace() && ShortGuid.TryParseGuid(controllerContext.HttpContext.Server.UrlDecode(id), out g)) return g;*/

                return BindGuid(id, controllerContext.HttpContext);

            }
    }
    public class GuidModelBinder : ShortGuidModelBinder {

    }
}