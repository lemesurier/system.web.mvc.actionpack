﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;

namespace System.Web.Mvc {
    public static class SimpleRestRouteCollectionExtensions {

        private static IDictionary<RestMethod, string[]> _RestMethodMethods;
        static SimpleRestRouteCollectionExtensions() {

            _RestMethodMethods = new Dictionary<RestMethod, string[]> { { RestMethod.Index, new[] { "get", "head" } }, { RestMethod.New, new[] { "get", "head" } }, { RestMethod.Create, new[] { "post" } }, { RestMethod.Show, new[] { "get", "head" } }, { RestMethod.Edit, new[] { "get", "head" } }, { RestMethod.Delete, new[] { "delete" } }, { RestMethod.Update, new[] { "put" } } };

        }


        public static RouteCollection Map<T>(this RouteCollection routes, string url, RestMethod[] methods) {
            MapRoute(routes, String.Empty, new[] { url }, typeof(T), methods, new RouteValueDictionary());
            return routes;
        }

        public static RouteCollection Index<T>(this RouteCollection routes, string url, object defaults = null, object constraints = null) {
            MapRoute(routes, String.Empty, new string[] { url }, typeof(T), new RestMethod[] { RestMethod.Index }, new RouteValueDictionary(defaults), new RouteValueDictionary(constraints));
            return routes;
        }
        public static RouteCollection New<T>(this RouteCollection routes, string url) {
            MapRoute(routes, String.Empty, new string[] { url }, typeof(T), new RestMethod[] { RestMethod.New }, new RouteValueDictionary());
            return routes;
        }
        public static RouteCollection Create<T>(this RouteCollection routes, string url, object defaults = null, object constraints = null) {
            MapRoute(routes, String.Empty, new string[] { url }, typeof(T), new RestMethod[] { RestMethod.Create }, new RouteValueDictionary(defaults), new RouteValueDictionary(constraints));
            return routes;
        }
        public static RouteCollection Create<T>(this RouteCollection routes, string[] url, object defaults = null, object constraints = null) {
            MapRoute(routes, String.Empty, url, typeof(T), new RestMethod[] { RestMethod.Create }, new RouteValueDictionary(defaults), new RouteValueDictionary(constraints));
            return routes;
        }
        public static RouteCollection Show<T>(this RouteCollection routes, string url, object defaults = null, object constraints = null) {
            MapRoute(routes, String.Empty, new string[] { url }, typeof(T), new RestMethod[] { RestMethod.Show }, new RouteValueDictionary(defaults), new RouteValueDictionary(constraints));
            return routes;
        }
        public static RouteCollection Edit<T>(this RouteCollection routes, string url, object defaults = null, object constraints = null) {
            MapRoute(routes, String.Empty, new string[] { url }, typeof(T), new RestMethod[] { RestMethod.Edit }, new RouteValueDictionary(defaults), new RouteValueDictionary(constraints));
            return routes;
        }
        public static RouteCollection Update<T>(this RouteCollection routes, string url) {
            MapRoute(routes, String.Empty, new string[] { url }, typeof(T), new RestMethod[] { RestMethod.Update }, new RouteValueDictionary());
            return routes;
        }
        public static RouteCollection Update<T>(this RouteCollection routes, string url, object defaults = null, object constraints = null) {
            MapRoute(routes, String.Empty, new string[] { url }, typeof(T), new RestMethod[] { RestMethod.Update }, new RouteValueDictionary(defaults), new RouteValueDictionary(constraints));
            return routes;
        }

        public static RouteCollection Delete<T>(this RouteCollection routes, string url, object defaults = null, object constraints = null) {
            MapRoute(routes, String.Empty, new string[] { url }, typeof(T), new RestMethod[] { RestMethod.Delete }, new RouteValueDictionary(defaults), new RouteValueDictionary(constraints));
            return routes;
        }

        public static RouteCollection Map<T>(this RouteCollection routes, string url, RestMethod method) {
            MapRoute(routes, String.Empty, new string[] { url }, typeof(T), new RestMethod[] { method }, new RouteValueDictionary());
            return routes;
        }

        public static Route MapRoute(this RouteCollection routes, string url, Type controller, RestMethod method) {
            return MapRoute(routes, String.Empty, new string[] { url }, controller, new RestMethod[] { method }, new RouteValueDictionary()).First();
        }
        

        public static Route MapRoute(this RouteCollection routes, string url, Type controller, RestMethod method, object defaults) {
            return MapRoute(routes, String.Empty, new string[] { url }, controller, new RestMethod[] { method }, new RouteValueDictionary(defaults)).First();
        }

        public static Route[] MapRoute(this RouteCollection routes, string url, Type controller, RestMethod[] methods, object defaults, object constraints) {
            return MapRoute(routes, String.Empty, new string[] { url }, controller, methods, new RouteValueDictionary(defaults), new RouteValueDictionary(constraints));
        }

        public static Route MapRoute(this RouteCollection routes, string url, Type controller, RestMethod method, RouteValueDictionary defaults) {
            return MapRoute(routes, String.Empty, new string[] { url }, controller, new RestMethod[] { method }, new RouteValueDictionary(defaults)).First();
        }

        /*public static Route MapRoute(this RouteCollection routes, string name, string url, Type controller, RestMethod RestMethod, object defaults) {
            return routes.MapRoute(name, new[] { url }, controller, new[] { RestMethod }, new RouteValueDictionary(defaults));
        }*/

        public static Route MapRoute(this RouteCollection routes, string name, string url, Type controller, RestMethod RestMethod, RouteValueDictionary defaults) {
            return MapRoute(routes, name, new string[] { url }, controller, new RestMethod[] { RestMethod }, new RouteValueDictionary(defaults)).First();
        }

        public static Route[] MapRoute(this RouteCollection routes, string[] urls, Type controller, RestMethod method) {
            return MapRoute(routes, String.Empty, urls, controller, new RestMethod[] { method }, new RouteValueDictionary());
        }

        public static Route[] MapRoute(this RouteCollection routes, string[] urls, Type controller, RestMethod method, RouteValueDictionary defaults) {
            return MapRoute(routes, String.Empty, urls, controller, new RestMethod[] { method }, new RouteValueDictionary(defaults));
        }

        public static Route[] MapRoute(this RouteCollection routes, string[] urls, Type controller, RestMethod method, object defaults) {
            return MapRoute(routes, String.Empty, urls, controller, new RestMethod[] { method }, new RouteValueDictionary(defaults));
        }

        public static Route[] MapRoute(this RouteCollection routes, string url, Type controller, RestMethod[] methods) {
            return MapRoute(routes, String.Empty, new string[] { url }, controller, methods, new RouteValueDictionary());
        }

        public static Route[] MapRoute(this RouteCollection routes, string[] urls, Type controller, RestMethod[] methods) {
            return MapRoute(routes, String.Empty, urls, controller, methods, new RouteValueDictionary());
        }

        public static Route[] MapRoute(this RouteCollection routes, string[] urls, Type controller, RestMethod[] methods, object defaults) {
            return MapRoute(routes, String.Empty, urls, controller, methods, new RouteValueDictionary(defaults));
        }

        public static Route[] MapRoute(this RouteCollection routes, string[] urls, Type controller, RestMethod[] methods, RouteValueDictionary defaults) {
            return MapRoute(routes, String.Empty, urls, controller, methods, new RouteValueDictionary(defaults));
        }

        public static Route[] MapRoute(this RouteCollection routes, string[] urls, Type controller, RestMethod[] methods, object defaults, object constraints) {
            return MapRoute(routes, String.Empty, urls, controller, methods, new RouteValueDictionary(defaults), new RouteValueDictionary(constraints));
        }

        private static Route[] MapRoute(RouteCollection routes, string name, string[] urls, Type controller, RestMethod[] methods, RouteValueDictionary defaults) {
            return MapRoute(routes, name, urls, controller, methods, defaults, new RouteValueDictionary());
        }


        private static Route[] MapRoute(RouteCollection routes, string name, string[] urls, Type controller, RestMethod[] methods, RouteValueDictionary defaults, RouteValueDictionary constraints) {
            List<Route> newRoutes = new List<Route>();
            if (typeof(ControllerBase).IsAssignableFrom(controller)) {
                string controllerName = controller.Name.EndsWith("Controller", StringComparison.InvariantCultureIgnoreCase) ? controller.Name.Substring(0, controller.Name.Length - 10) : controller.Name;

                foreach (string url in urls) {
                    foreach (RestMethod RestMethod in methods) {
                        string n = String.Format("{0}_{1}", controller.Name, RestMethod.ToString());
                        int i = 0;
                        while (routes[n] != null) {
                            n += i++;
                        }
                        string u = url;
                        RouteValueDictionary d = new RouteValueDictionary(defaults) { { "controller", controllerName }, { "action", RestMethod.ToString() } };
                        RouteValueDictionary c = new RouteValueDictionary(constraints) { { "httpMethod", new HttpMethodConstraint(_RestMethodMethods[RestMethod]) } };
                        
                        // check for optional tokens
                        /*UriTemplate t = new UriTemplate(u);
                        foreach (string param in t.PathSegmentVariableNames) {
                            string newParam = param.Trim(new[] { '-', '+', '?' });
                            if (param.StartsWith("-")) {
                                if (!d.ContainsKey(newParam)) d.Add(newParam, UrlParameter.Optional);
                            }
                            if (param.EndsWith("+")) {
                                if (d.ContainsKey(newParam) && d[newParam] == UrlParameter.Optional) d.Remove(newParam);
                            }
                            u = u.Replace("{" + param + "}", "{" + newParam + "}");
                        }*/

                        /*if (!u.Contains(".")) {
                            u += ".{extension}";
                            d.Add("extension", UrlParameter.Optional);
                        }*/

                        Route r = routes.MapRoute(n, u);
                        
                        r.Defaults = d;
                        r.Constraints = c;
                        //r.Constraints = new RouteValueDictionary(new { httpMethod = new HttpMethodConstraint(_RestMethodMethods[RestMethod]) });
                        newRoutes.Add(r);
                    }
                }
            }
            return newRoutes.ToArray();
        }
    }
    public class HttpMethodConstraint : System.Web.Routing.HttpMethodConstraint {
        public HttpMethodConstraint(string[] allowedMethods)
            : base(allowedMethods) {

        }
        public HttpMethodConstraint(string allowedMethod) : this(new[] { allowedMethod }) {

        }
        protected override bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection) {

            var verb = httpContext.Request.HttpMethod;

            if ("POST".Equals(verb, StringComparison.InvariantCultureIgnoreCase) && !String.IsNullOrWhiteSpace(httpContext.Request.Params["_method"])) {
                verb = httpContext.Request.Params["_method"].ToUpper();
            }

            if (routeDirection == RouteDirection.IncomingRequest) {
                return this.AllowedMethods.Any(httpMethod => String.Equals(httpMethod, verb, StringComparison.OrdinalIgnoreCase));
                //return this.AllowedMethods.Any(httpMethod => String.Equals(httpMethod, httpContext.Request.RequestType, StringComparison.OrdinalIgnoreCase) || (String.Equals("post", httpContext.Request.HttpMethod, StringComparison.InvariantCultureIgnoreCase) && String.Equals(httpMethod, httpContext.Request.Form["_method"], StringComparison.OrdinalIgnoreCase)) || String.Equals(httpMethod, httpContext.Request.HttpMethod, StringComparison.OrdinalIgnoreCase));
            }
            return base.Match(httpContext, route, parameterName, values, routeDirection);
        }
    }
    public enum RestMethod {
        Index,
        New,
        Create,
        Show,
        Edit,
        Update,
        Delete
    }
}
