﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;

// http://blogorama.nerdworks.in/entry-EnablingJSONPcallsonASPNETMVC.aspx

namespace System.Web.Mvc {
    /// <summary>
    /// Renders result as JSON and also wraps the JSON in a call
    /// to the callback function specified in "JsonpResult.Callback".
    /// </summary>
    public class JsonpResult : JsonResult {
        /// <summary>
        /// Gets or sets the javascript callback function that is
        /// to be invoked in the resulting script output.
        /// </summary>
        /// <value>The callback function name.</value>
        public string Callback { get; set; }

        public string Content { get; set; }

        /// <summary>
        /// Enables processing of the result of an action method by a
        /// custom type that inherits from <see cref="T:System.Web.Mvc.ActionResult"/>.
        /// </summary>
        /// <param name="context">The context within which the
        /// result is executed.</param>
        public override void ExecuteResult(ControllerContext context) {
            if (context == null)
                throw new ArgumentNullException("context");

            HttpResponseBase response = context.HttpContext.Response;

            // set initial content type
            response.ContentType = ContentType.IsNullOrWhiteSpace() ? Format.Json.ToMimeType() : ContentType;

            if (ContentEncoding != null)
                response.ContentEncoding = ContentEncoding;

            if (Callback == null || Callback.Length == 0)
                Callback = context.HttpContext.Request.QueryString["callback"];

            // callback requires javascript type
            if (!Callback.IsNullOrWhiteSpace()) response.ContentType = Format.JavaScript.ToMimeType();

            if (Data != null) {

                response.Write(Callback + "(" + context.Controller.RenderJson(Data, true) + ");");

            } else if (Content != null) {
                response.Write(Callback + "(" + Content + ");");
            }
        }
    }

    public class JsonRequestBehaviorAttribute : FilterAttribute, IResultFilter {
        private JsonRequestBehavior Behavior { get; set; }

        public JsonRequestBehaviorAttribute(JsonRequestBehavior behavior) {
            Behavior = behavior;
        }

        public void OnResultExecuting(ResultExecutingContext filterContext) {
            var result = filterContext.Result as JsonResult;

            if (result != null) {
                result.JsonRequestBehavior = Behavior;
            }
        }

        public void OnResultExecuted(ResultExecutedContext filterContext) {
            //throw new NotImplementedException();
        }
    }

    public class JsonpAttribute : FilterAttribute, IActionFilter {
        public void OnActionExecuted(ActionExecutedContext filterContext) {
            if (filterContext == null)
                throw new ArgumentNullException("filterContext");

            //
            // see if this request included a "callback" querystring parameter
            //
            string callback = filterContext.HttpContext.Request.QueryString["callback"];
            if(callback.IsNullOrWhiteSpace()) return; // callback required
            
            // Content result as JSON
            ContentResult contentResult = filterContext.Result as ContentResult;
            if (contentResult != null && Format.Json.ToMimeType().Equals(contentResult.ContentType, StringComparison.InvariantCultureIgnoreCase)) {
                filterContext.Result = new JsonpResult {
                    ContentEncoding = contentResult.ContentEncoding,
                    ContentType = contentResult.ContentType,
                    Content = contentResult.Content,
                    Callback = callback
                };
                return;
            }

            // JSON result
            JsonResult jsonResult = filterContext.Result as JsonResult;
            if (jsonResult != null) {

                filterContext.Result = new JsonpResult {
                    ContentEncoding = jsonResult.ContentEncoding,
                    ContentType = jsonResult.ContentType,
                    Data = jsonResult.Data,
                    Callback = callback
                };
                /*throw new InvalidOperationException("JsonpFilterAttribute must be applied only " +
                    "on controllers and actions that return a JsonResult object.");*/

                return; // just end
            }

            // Status result on a JSON request
            HttpStatusResult statusResult = filterContext.Result as HttpStatusResult;
            if (statusResult != null && FormatHelper.GetFormat(filterContext.HttpContext.Request) == Format.Json) {
                filterContext.Result = new JsonpResult {
                    ContentType = Format.Json.ToMimeType(),
                    Data = new {
                        status = new {
                            code = statusResult.StatusCode,
                            description = statusResult.Message
                        }
                    },
                    Callback = callback
                };
            }

            // Status result on a JSON request
            HttpStatusCodeResult statusCodeResult = filterContext.Result as HttpStatusCodeResult;
            if (statusCodeResult != null && FormatHelper.GetFormat(filterContext.HttpContext.Request) == Format.Json) {
                filterContext.Result = new JsonpResult {
                    ContentType = Format.Json.ToMimeType(),
                    Data = new {
                        status = new {
                            code = statusCodeResult.StatusCode
                        }
                    },
                    Callback = callback
                };
            }

        }


        public void OnActionExecuting(ActionExecutingContext filterContext) {
            //throw new NotImplementedException();
        }
    }

    public static class JsonControllerExtensions {
        public static string RenderJson(this ControllerBase @this, object data, bool jsonp = true) {
            if (data == null) return null;
            return JsonConvert.SerializeObject(data, new JsonSerializerSettings {
                      NullValueHandling = NullValueHandling.Ignore,
                      DateFormatHandling = DateFormatHandling.IsoDateFormat
                });
            //return Jil.JSON.Serialize(data, new Jil.Options(jsonp: jsonp, dateFormat: Jil.DateTimeFormat.ISO8601));
        }
    }
}
