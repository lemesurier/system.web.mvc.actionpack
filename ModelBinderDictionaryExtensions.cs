﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace System.Web.Mvc {
    public static class ModelBinderDictionaryExtensions {
        public static ModelBinderDictionary For<T>(this ModelBinderDictionary d, IModelBinder binder) {
            d[typeof(T)] = binder;
            return d;
        }
        public static ModelBinderDictionary Register(this ModelBinderDictionary d, Type t, IModelBinder b) {
            d[t] = b;
            return d;
        }
        public static ModelBinderDictionary Register(this ModelBinderDictionary d, IDictionary<Type, IModelBinder> b) {
            foreach (Type t in b.Keys) {
                d.Register(t, b[t]);
            }
            return d;
        }
        public static ModelBinderDictionary Register(this ModelBinderDictionary d, ModelBinderDictionary b) {
            foreach (Type t in b.Keys) {
                d.Register(t, b[t]);
            }
            return d;
        }
    }
}
