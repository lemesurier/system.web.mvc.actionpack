﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;

namespace System.Web.Mvc {
    public class GuidRouteConstraint : IRouteConstraint {

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection) {
            if (values.ContainsKey(parameterName)) {
                string stringValue = values[parameterName] as string;

                //httpContext.Trace.Write("binding for guid \"" + stringValue + "\"");
                return GuidModelBinder.BindGuid(stringValue).HasValue;

                /*if (!string.IsNullOrEmpty(stringValue)) {
                    ShortGuid guidValue;

                    return ShortGuid.TryParse(stringValue, out guidValue) && (guidValue.ToGuid() != Guid.Empty);
                }*/
            }

            return false;
        }
    }
}
