﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.Mvc {
    public static class ActionPackHtmlHelperExtensions {
        public static IHtmlString File(this HtmlHelper @this, string path) {
            var filePath = HttpContext.Current.Server.MapPath(path);
            if (!System.IO.File.Exists(filePath)) return null;
            return new HtmlString(System.IO.File.ReadAllText(filePath));
        }
    }
}
