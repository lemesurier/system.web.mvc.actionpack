﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace System.Web.Mvc {
	public class HttpConflictResult : HttpStatusResult {


        public HttpConflictResult()
            : this(null) {

        }

        public HttpConflictResult(string message)
            : base(HttpStatusCode.Conflict, message) {
        }

		public HttpConflictResult(object json)
			: base(HttpStatusCode.Conflict, json) {
		}

        public override void ExecuteResult(ControllerContext context) {
            base.ExecuteResult(context);

            //if(!string.IsNullOrWhiteSpace(this.Message)) context.HttpContext.Response.Write(this.Message);

        }
    }
}
