﻿using System;

using System.Collections.Generic;

using System.Linq;

using System.Text;

using System.IO;
using System.Net;



namespace System.Web.Mvc {

    public static class RenderPartialViewControllerExtensions {

        public static string RenderView(this Controller controller) {
            return controller.RenderView(null, null, null);
        }

        public static string RenderView(this Controller controller, string viewName) {
            return controller.RenderView(viewName, null, null);
        }

        public static string RenderView(this Controller controller, object model) {
            return controller.RenderView(null, null, model);
        }

        public static string RenderView(this Controller controller, string viewName, string masterName, object model) {

            if (string.IsNullOrEmpty(viewName))
                viewName = controller.ControllerContext.RouteData.GetRequiredString("action");

            controller.ViewData.Model = model;

            using (StringWriter sw = new StringWriter()) {
                ViewEngineResult viewResult = ViewEngines.Engines.FindView(controller.ControllerContext, viewName, null);
                ViewContext viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.ToString();
            }
        }

        public static string RenderView(this Controller controller, ViewResult view) {

            IView resolvedView = view.View ?? (view.ViewEngineCollection ?? ViewEngines.Engines).FindView(controller.ControllerContext, view.ViewName, view.MasterName).View;

            using (StringWriter sw = new StringWriter()) {

                ViewContext viewContext = new ViewContext(controller.ControllerContext, resolvedView, view.ViewData, view.TempData, sw);
                resolvedView.Render(viewContext, sw);

                return sw.ToString();
            }
        }

        public static string RenderPartialView(this Controller controller) {
            return controller.RenderPartialView(null, null);
        }

        public static string RenderPartialViewToString(this Controller controller, string viewName) {
            return controller.RenderPartialView(viewName, null);
        }

        public static string RenderPartialView(this Controller controller, object model) {
            return controller.RenderPartialView(null, model);
        }

        public static string RenderPartialView(this Controller controller, string viewName, object model) {
            if (string.IsNullOrEmpty(viewName))
                viewName = controller.ControllerContext.RouteData.GetRequiredString("action");

            controller.ViewData.Model = model;

            using (StringWriter sw = new StringWriter()) {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.ToString();
            }
        }

        public static ContentResult Content(this Controller controller, string content, Format format) {
            return new ContentResult { Content = content, ContentType = format.ToMimeType() };
        }

        public static HttpStatusCodeResult HttpOK(this Controller controller) {
            return new HttpStatusCodeResult((int)HttpStatusCode.OK);
        }

        public static HttpCreatedResult HttpCreated(this Controller controller, Uri location) {
            if (location == null) return new HttpCreatedResult();
            return new HttpCreatedResult(location.IsAbsoluteUri ? location : new Uri(controller.Request.Url, location));
        }

        public static HttpCreatedResult HttpCreated(this Controller controller, string path) {
            return controller.HttpCreated(new Uri(VirtualPathUtility.ToAbsolute(path), UriKind.Relative));
        }

        public static HttpBadRequestResult HttpBadRequest(this Controller controller, string message) {
            return new HttpBadRequestResult(message);
        }

    }

}

