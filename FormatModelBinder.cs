﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace System.Web.Mvc {
    public class FormatModelBinder : CustomModelBinderAttribute, IModelBinder {

        public FormatModelBinder() {

        }


        // try to pull out the page key parameter from route data and then retrieve the sitemapnode from the current sitemap.  This will return null if the associated node isn't in this site
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext) {

            return Bind(controllerContext, bindingContext);
            

        }

        public static Format Bind(ControllerContext controllerContext, ModelBindingContext bindingContext) {
            // try to get from route data
            object obj;
            if (controllerContext.RouteData.Values.TryGetValue(bindingContext.ModelName, out obj)) {
                if (obj is Format) {
                    return (Format)obj;
                }
            }

            return FormatHelper.GetFormat(controllerContext.HttpContext.Request);
        }

        public override IModelBinder GetBinder() {
            return this;
        }

    }
}
