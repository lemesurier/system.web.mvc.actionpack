﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.Mvc {
    public class JsonJilResult : JsonResult {

        public JsonJilResult() {

        }

        public JsonJilResult(object data) {
            this.Data = data;
        }
        /*public Encoding ContentEncoding { get; set; }
        public string ContentType { get; set; }
        public object Data { get; set; }*/

        // in general, this won't get executed for jsonp, jsonp response replaces and serializes to string first
        private bool _jsonp = false;

        public bool Jsonp {
            get { return _jsonp; }
            set { _jsonp = value; }
        }

        /*public JsonNetResult() {
            SerializerSettings = new JsonSerializerSettings();
        }*/

        public override void ExecuteResult(ControllerContext context) {
            if (context == null)
                throw new ArgumentNullException("context");

            // NEED TO IMPLEMENT ALLOWGET FILTERING?

            HttpResponseBase response = context.HttpContext.Response;

            response.ContentType = !string.IsNullOrEmpty(ContentType)
              ? ContentType
              : "application/json";

            if (ContentEncoding != null)
                response.ContentEncoding = ContentEncoding;

            if (Data != null) {

                // Request behavior?

                // serialize straight to the response stream
                Jil.JSON.Serialize(this.Data,response.Output, new Jil.Options(jsonp: _jsonp, dateFormat: Jil.DateTimeFormat.ISO8601, excludeNulls: true));

                response.AppendHeader("X-JSON-Serializer", "Jil");
            }
        }
    }


    public class JsonJilResultAttribute : ActionFilterAttribute {
        public override void OnActionExecuted(ActionExecutedContext filterContext) {

            // don't replace jsonnet
            var jsonNetResult = filterContext.Result as JsonNetResult;
            if (null == jsonNetResult) {

                var jsonResult = filterContext.Result as JsonResult;

                if (jsonResult != null) {
                    filterContext.Result = new JsonJilResult {
                        ContentEncoding = jsonResult.ContentEncoding,
                        ContentType = jsonResult.ContentType,
                        Data = jsonResult.Data,
                        JsonRequestBehavior = jsonResult.JsonRequestBehavior
                    };
                }

            }

            base.OnActionExecuted(filterContext);
        }
    }
}
