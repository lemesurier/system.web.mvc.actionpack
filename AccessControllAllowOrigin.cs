﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.Mvc {
    [AttributeUsage(AttributeTargets.Class|AttributeTargets.Method, AllowMultiple = false)]
    public class AccessControlAllowOriginAttribute : ActionFilterAttribute {

        // NEED TO ADD WHITELISTING

        public override void OnResultExecuted(ResultExecutedContext filterContext) {

            // no point in get - make this an option?
            //if("GET".Equals(filterContext.HttpContext.Request.RequestType, StringComparison.InvariantCultureIgnoreCase)) return;

            var origin = filterContext.HttpContext.Request.Headers["Origin"];
            if(origin.IsNullOrWhiteSpace()) origin = "*";

            filterContext.HttpContext.Response.AppendHeader("Access-Control-Allow-Origin", origin);
            base.OnResultExecuted(filterContext);
        }
    }
}