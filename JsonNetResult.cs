﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.Mvc {
    public class JsonNetResult : JsonResult {
        /*public Encoding ContentEncoding { get; set; }
        public string ContentType { get; set; }
        public object Data { get; set; }*/

        public JsonSerializerSettings SerializerSettings { get; set; }
        public Formatting Formatting { get; set; }

        /*public JsonNetResult() {
            SerializerSettings = new JsonSerializerSettings();
        }*/

        public override void ExecuteResult(ControllerContext context) {
            if (context == null)
                throw new ArgumentNullException("context");

            // NEED TO IMPLEMENT ALLOWGET FILTERING?

            HttpResponseBase response = context.HttpContext.Response;

            response.ContentType = !string.IsNullOrEmpty(ContentType)
              ? ContentType
              : "application/json";

            if (ContentEncoding != null)
                response.ContentEncoding = ContentEncoding;

            if (Data != null) {
                JsonTextWriter writer = new JsonTextWriter(response.Output) { Formatting = Formatting };

                JsonSerializer serializer = JsonSerializer.Create(SerializerSettings ?? new JsonSerializerSettings {
                      NullValueHandling = NullValueHandling.Ignore,
                      DateFormatHandling = DateFormatHandling.IsoDateFormat
                });
                serializer.Serialize(writer, Data);

                writer.Flush();

                response.AppendHeader("X-JSON-Serializer", "JSON.NET");
            }
        }
    }

    public class JsonNetResultAttribute : ActionFilterAttribute {
        public override void OnActionExecuted(ActionExecutedContext filterContext) {

            // don't replace jsonnet
            var jsonNetResult = filterContext.Result as JsonNetResult;
            if (null == jsonNetResult) {

                var jsonResult = filterContext.Result as JsonResult;

                if (jsonResult != null) {
                    filterContext.Result = new JsonNetResult {
                        ContentEncoding = jsonResult.ContentEncoding,
                        ContentType = jsonResult.ContentType,
                        Data = jsonResult.Data,
                        JsonRequestBehavior = jsonResult.JsonRequestBehavior
                    };
                }

            }

            base.OnActionExecuted(filterContext);
        }
    }
}
