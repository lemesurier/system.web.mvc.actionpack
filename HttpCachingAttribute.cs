﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.Mvc {
    public class HttpCachingAttribute : ActionFilterAttribute {

        private TimeSpan duration = TimeSpan.Zero;

        public TimeSpan Duration {
            get { return duration; }
            set { duration = value; }
        }

        public int Seconds {
            get {
                return (int) Duration.TotalSeconds;
            }
            set {
                this.Duration = TimeSpan.FromSeconds(value);
            }
        }

        public HttpCachingAttribute() {

        }

        public HttpCachingAttribute(int duration) {
            this.Seconds = duration;
        }

        public HttpCachingAttribute(TimeSpan duration) {
            this.Duration = duration;
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext) {
            base.OnResultExecuted(filterContext);

            // output caching
            if (this.Duration > TimeSpan.Zero) {
                filterContext.HttpContext.Response.Cache.SetCacheability(HttpCacheability.Public);
                filterContext.HttpContext.Response.Cache.SetSlidingExpiration(true);
                filterContext.HttpContext.Response.Cache.SetMaxAge(this.duration);
                //filterContext.HttpContext.Response.AppendHeader("Cache-Control", "public, max-age={0}".FormatWith((int)this.duration.TotalSeconds)); // HTTP 1.1.
                //filterContext.HttpContext.Response.AddHeader("Cache-Control", "public");
                //filterContext.HttpContext.Response.AddHeader("Cache-Control", "max-age=" + (int)this.Duration.TotalSeconds);
                //filterContext.HttpContext.Response.AppendHeader("Pragma", "no-cache"); // HTTP 1.0.
                //filterContext.HttpContext.Response.AppendHeader("Expires", "0");

            } else {
                //filterContext.HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
               // filterContext.HttpContext.Response.Cache.SetNoStore();
                filterContext.HttpContext.Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
                filterContext.HttpContext.Response.AppendHeader("Pragma", "no-cache"); // HTTP 1.0.
                filterContext.HttpContext.Response.AppendHeader("Expires", "0");
            }
        }
    }
}
